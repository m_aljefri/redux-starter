import React, { Component } from "react";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchWather } from '../actions/index'

// console.log(this.props.fetchWeather('ali'))
class SearchBar extends Component {
  constructor(props){
    super(props);

    this.state = {
      term: ''
    }

    this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }
  onInputChange(event){
    this.setState({term: event.target.value});
  }

  onFormSubmit(event){
    event.preventDefault();

    this.props.fetchWather(this.state.term);
    this.setState({ term: '' });
    // on submit search of city weather 
  }
  render() {
    return(
      <form onSubmit={this.onFormSubmit}>
        <div className="input-group">
          <input 
            type="text" 
            className="form-control"
            value={this.state.term}
            placeholder="Search of a city"
            onChange={this.onInputChange} />
          <span className="input-group-btn">
            <button 
              type="submit" 
              className="btn btn-secondry">Search</button>
          </span>
        </div>
      </form>
    )
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({ fetchWather }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar)
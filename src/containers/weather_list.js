import React, { Component } from 'react'
import { connect } from 'react-redux'

class WeatherList extends Component {
  constructor(props){
    super(props);
  }

  renderWeather(data,index){
    return(
      <tr key={index}>
        <td>{data.data.city.name}</td>
        <td>{data.data.list[0].weather[0].main}</td>
        <td>{data.data.list[0].wind.speed}</td>
        <td>
          {
          (data.data.list[0].wind.speed>2) ?
           <span>hight wind</span> : 
           <span>min wind</span>
          }
        </td>
      </tr>
    );
  }

  componentDidUpdate(){
  }
  render(){
    if(this.props.weather){
      return (
        <table className="table table-hover">
          <thead>
            <tr>
              <th>City</th>
              <th>Weather</th>
              <th>Wind speed</th>
              <th>Huimis</th>
            </tr>
          </thead>
          <tbody>
            {this.props.weather.map(this.renderWeather)}
          </tbody>
        </table>
      )
    }
    return <p>Searching first..</p>
  }

}

function mapStateToProps({ weather }){
  return { weather };
}

export default connect(mapStateToProps)(WeatherList);

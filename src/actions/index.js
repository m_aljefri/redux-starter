import axios from 'axios'

const API_KEY = 'bace1b87967ede95839972200c862aeb';
const BASE_URL = `http://samples.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWather(city){
  const URL = `${BASE_URL}&q=${city},us`;
  const result = axios.get(URL);

  return{
    type: FETCH_WEATHER,
    payload: result
  };
}
